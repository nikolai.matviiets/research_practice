# research_practice

## Description

The aim of the project was to train a DNABERT model to predict transcription factor bidning sites in bacterial genomes. 

## Installation

Follow steps form https://github.com/jerryji1993/DNABERT to install DNABERT and all needed dependencies.

## Pre-training the model

1. Use the `scripts/get_sequences_from_genome.py` to generate sequences needed to pre-train the model. Provide a path to the genome path as the first argument.
2. Turn generated sequences to k-mers using `scripts/turn_to_kmers.py`
3. Run `dnabert_bash/pre_train.sh` to pre-train the model. Provide needed variables inside the script
4. NB: can take several days to complete 

## Prepare examples from ChIP-seq data to fine-tune the model

1. Download proChIPdb database from this website: https://prochipdb.org/
2. Run `scripts/get_examples_from_chipseq_db.py` to generate positive and negative examples
3. Combine the examples you need by concatenating files containing these examples into examples.tsv file
4. Turn generated sequences to k-mers using `scripts/turn_to_kmers.py`
5. Split the data in train and dev sets using `scripts/split_data_in_train_and_test.py`

## Prepare examples from DAP-seq data to fine-tune the model

1. Run `scripts/get_examples_from_dapseq_db_new.py` to generate positive and negative examples
2. Combine the examples you need by concatenating files containing these examples into examples.tsv file
3. Turn generated sequences to k-mers using `scripts/turn_to_kmers.py`
4. Split the data in train and dev sets using `scripts/split_data_in_train_and_test.py`

## Fine-tune a model
1. Go to the output folder 
2. Turn generated sequences to k-mers using `scripts/turn_to_kmers.py`
3. Split the data in train and dev sets using `scripts/split_data_in_train_and_test.py`
4. Run `dnabert_bash/fine_tune.sh` to pre-train the model. Provide needed variables inside the script
5. Check eval_results.txt file to see the validation metrics.

## Make predictions
1. Run `dnabert_bash/predict.sh` on a file with examples. The file must be called dev.tsv
2. In the output folder check results in the pred_results.npy file

## Other scripts
1. Run `scripts/count_intersections_in_chipseq_db.py` to calculate how many peaks in ChIP-seq data do intersect
1. Run `scripts/count_intersections_in_dapseq_db.py` to calculate how many peaks in DAP-seq data do intersect