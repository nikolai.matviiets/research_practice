import os
import sys

PATH_TO_PEAKS = 'positions'
PATH_TO_GENOME = 'sequence'


def read_center_coordinates(regulator_path):
    center_coordinates = []
    with open(regulator_path, 'r') as f:
        # skip header
        f.readline()
        for line in f:
            # get field with name binding_peak_center
            position = line.split(',')[-1]
            center_coordinates.append(position.split('.')[0])
    return center_coordinates


def main():
    path_to_positions = sys.argv[1]
    organism_name = sys.argv[2]
    output_dir = sys.argv[3]
    padding = int(sys.argv[4])

    summits = []
    peaks_count = {}
    intercepting_summits = {}
    interceptions_count = 0

    for pos_file in os.listdir(f"{path_to_positions}"):
        path_to_pos_file = f"{path_to_positions}/{pos_file}"
        regulator = pos_file.split("_positions.csv")[0]
        if regulator.startswith('RpoB'):
            regulator = 'RpoB'
        if regulator.startswith('mixed'):
            regulator = regulator.replace('TFs-pool', '')
        cent_coordinates = read_center_coordinates(path_to_pos_file)
        for pos in cent_coordinates:
            summits.append(Summit(regulator, int(pos)))

    for index1, summit1 in enumerate(summits):
        for index2, summit2 in enumerate(summits[index1 + 1:]):
            if summit1.position in range(summit2.position - padding,
                                         summit2.position + padding):
                interceptions_count += 1
                if summit1.gene_name in intercepting_summits.keys():
                    intercepting_summits[summit1.gene_name].append(
                        (summit1, summit2))
                else:
                    intercepting_summits[summit1.gene_name] = [
                        (summit1, summit2)]
                if summit2.gene_name in intercepting_summits.keys():
                    intercepting_summits[summit2.gene_name].append(
                        (summit2, summit1))
                else:
                    intercepting_summits[summit2.gene_name] = [
                        (summit2, summit1)]

    with open(os.path.join(output_dir, organism_name + f"_{padding}_intercepts_count.tsv"),
              'a') as summary_file:
        if len(summits) != 0:
            ratio = interceptions_count / len(summits)
        else:
            ratio = 0

        summary_file.write(f"{organism_name}:\n")
        summary_file.write(f"Total\t{interceptions_count}\t{len(summits)}\t"
                           f"{round(ratio, 5)}\n")

        for gene_name in peaks_count.keys():
            num_of_peaks = peaks_count[gene_name]
            if gene_name in intercepting_summits.keys():
                num_of_interceptions = len(intercepting_summits[gene_name])
            else:
                num_of_interceptions = 0
            ratio_for_regulator = num_of_interceptions / num_of_peaks
            summary_file.write(
                f"{gene_name}\t{num_of_interceptions}\t{num_of_peaks}\t"
                f"{round(ratio_for_regulator, 5)}\n")


class Summit:
    def __init__(self, gene_name, position):
        self.gene_name = gene_name
        self.position = position


if __name__ == '__main__':
    main()
