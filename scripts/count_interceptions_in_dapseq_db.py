import os
import sys


def main():
    organism_dir = sys.argv[1]
    output_dir = sys.argv[2]
    padding = int(sys.argv[3])
    ending_of_fn_with_peaks = sys.argv[4]

    # only for chromosomes not to look into plasmids
    chromosome_genomes = ['NC_013595.1', 'AL645882.2', 'NC_016109.1']

    # splits = organism_dir.split('-', 2)[2].split('_', 2)
    splits = os.path.basename(organism_dir).split('_', 2)
    organism_name = splits[0] + '_' + splits[1]

    summits = []
    peaks_count = {}
    intercepting_summits = {}
    interceptions_count = 0

    for gene_dir in os.listdir(organism_dir):
        if gene_dir.startswith('.'):
            continue
        splits = gene_dir.split('_')
        if len(splits) == 0:
            continue
        if not splits[0].startswith('SCO'):
            continue

        gene_name = splits[0]
        gene_dir_path = f"{organism_dir}/{gene_dir}"
        path_to_peak_file = ''

        for file in os.listdir(gene_dir_path):
            if file.endswith(ending_of_fn_with_peaks):
                path_to_peak_file = f"{gene_dir_path}/{file}"
                break

        with open(path_to_peak_file, 'r') as f:
            for line in f:
                accession_number = line.split('\t')[0]
                if accession_number not in chromosome_genomes:
                    continue

                start = line.split('\t')[1]
                rel_summit_pos = line.split('\t')[9]
                summit_pos = int(start) + int(rel_summit_pos)
                summits.append(Summit(gene_name, summit_pos))
                if gene_name in peaks_count.keys():
                    peaks_count[gene_name] += 1
                else:
                    peaks_count[gene_name] = 1

    for index1, summit1 in enumerate(summits):
        for index2, summit2 in enumerate(summits[index1+1:]):
            if summit1.position in range(summit2.position - padding,
                                         summit2.position + padding):
                interceptions_count += 1
                if summit1.gene_name in intercepting_summits.keys():
                    intercepting_summits[summit1.gene_name].append((summit1, summit2))
                else:
                    intercepting_summits[summit1.gene_name] = [(summit1, summit2)]
                if summit2.gene_name in intercepting_summits.keys():
                    intercepting_summits[summit2.gene_name].append((summit2, summit1))
                else:
                    intercepting_summits[summit2.gene_name] = [(summit2, summit1)]

    if 'filt' in ending_of_fn_with_peaks:
        suffix = '_filt'
    else:
        suffix = ''

    with open(os.path.join(output_dir, organism_name + f"_{padding}_intercepts_count{suffix}.tsv"),
              'a') as summary_file:
        if len(summits) != 0:
            ratio = interceptions_count / len(summits)
        else:
            ratio = 0

        summary_file.write(f"{organism_name}:\n")
        summary_file.write(f"Total\t{interceptions_count}\t{len(summits)}\t"
                           f"{round(ratio, 5)}\n")

        for gene_name in peaks_count.keys():
            num_of_peaks = peaks_count[gene_name]
            if gene_name in intercepting_summits.keys():
                num_of_interceptions = len(intercepting_summits[gene_name])
            else:
                num_of_interceptions = 0
            ratio_for_regulator = num_of_interceptions / num_of_peaks
            summary_file.write(
                f"{gene_name}\t{num_of_interceptions}\t{num_of_peaks}\t"
                f"{round(ratio_for_regulator, 5)}\n")


class Summit:
    def __init__(self, gene_name, position):
        self.gene_name = gene_name
        self.position = position


if __name__ == '__main__':
    main()
