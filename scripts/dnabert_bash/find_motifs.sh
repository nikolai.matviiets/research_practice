#!/bin/bash

export KMER=6
export MODEL_PATH='path to the pre-trained model'
export DATA_PATH='path to files with examples'
export PREDICTION_PATH='path to directory with predictions'
export MOTIF_PATH='path to output directory'

python ../../DNABERT/motif/find_motifs.py \
    --data_dir $DATA_PATH \
    --predict_dir $PREDICTION_PATH \
    --window_size 24 \
    --min_len 5 \
    --pval_cutoff 0.005 \
    --min_n_motif 3 \
    --align_all_ties \
    --save_file_dir $MOTIF_PATH \
    --verbose


