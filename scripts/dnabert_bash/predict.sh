#!/bin/bash

export KMER=6
export MODEL_PATH='path to the pre-trained model'
export DATA_PATH='path to files with examples'
export PREDICTION_PATH='path to output directory'

python ../../DNABERT/examples/run_finetune_no_eval.py \
    --model_type dna \
    --tokenizer_name=dna$KMER \
    --model_name_or_path $MODEL_PATH \
    --task_name dnaprom \
    --do_predict \
    --data_dir $DATA_PATH  \
    --max_seq_length 101 \
    --per_gpu_pred_batch_size=128   \
    --output_dir $MODEL_PATH \
    --predict_dir $PREDICTION_PATH \
    --n_process 48
