import os
import sys

from shuffle import dinuclShuffle

PATH_TO_PEAKS = 'positions'
PATH_TO_GENOME = 'sequence'


def read_center_coordinates(regulator_path):
    center_coordinates = []
    with open(regulator_path, 'r') as f:
        # skip header
        f.readline()
        for line in f:
            # get field with name binding_peak_center
            center_coordinates.append(line.split(',')[-1])
    return center_coordinates


def read_genome(sample_dir):
    genome = ''
    with open(os.path.join(f"{sample_dir}/{PATH_TO_GENOME}/sequence.fasta"),
              'r') as f:
        # skip header
        f.readline()
        # read line by line
        for line in f:
            # add line to genome sequence
            genome += line.strip()
    return genome


def generate_sequences(center_coordinates, genome, file, counts):
    pos_sequences = []
    neg_sequences = []
    for center in center_coordinates:
        center = center.split('.')[0]
        center = center.strip()
        try:
            center = int(center)
        except ValueError:
            counts["bad_metadata"] = counts["bad_metadata"] + 1
            continue
        start = center - 50
        end = center + 50
        pos_seq = genome[start:end + 1]
        try:
            neg_seq = dinuclShuffle(pos_seq)
        except KeyError:
            counts["bad_sequence"] = counts["bad_sequence"] + 1
            continue
        pos_sequences.append(pos_seq)
        neg_sequences.append(neg_seq)
        counts["good"] = counts["good"] + 1
    return pos_sequences, neg_sequences, counts


def write_sequences_to_file(pos_sequences, neg_sequences, out_dir, file_name):
    with open(os.path.join(out_dir, file_name), 'a') as f:
        for seq in pos_sequences:
            f.write(seq + ' 1' + '\n')
        for seq in neg_sequences:
            f.write(seq + ' 0' + '\n')


def main():
    root_path = sys.argv[1]
    output_dir = sys.argv[2]
    counts = {"good": 0, "bad_metadata": 0, "bad_sequence": 0}
    for organism_dir in os.listdir(f'{root_path}'):
        path_to_organism = os.path.join(root_path, organism_dir)
        for sample_dir in os.listdir(path_to_organism):
            if sample_dir == 'TF_list.csv':
                continue
            path_to_sample = f"{path_to_organism}/{sample_dir}"
            path_to_sample_peaks = f"{path_to_sample}/{PATH_TO_PEAKS}"
            for pos_file in os.listdir(f"{path_to_sample_peaks}"):
                if pos_file.endswith(".csv"):
                    path_to_pos_file = f"{path_to_sample_peaks}/{pos_file}"
                    regulator = pos_file.split("_positions.csv")[0]
                    cent_coordinates = read_center_coordinates(path_to_pos_file)
                    genome_file = read_genome(path_to_sample)
                    pos_sequences, neg_sequences, counts = generate_sequences(
                        cent_coordinates, genome_file, path_to_pos_file, counts)
                    file_name = f'{organism_dir}__{sample_dir}__{regulator}.tsv'
                    file_name = file_name.replace(" ", "_")
                    file_name = file_name.replace("(", "").replace(")", "")
                    write_sequences_to_file(pos_sequences, neg_sequences,
                                            output_dir, file_name)
    print(counts)


if __name__ == '__main__':
    main()
