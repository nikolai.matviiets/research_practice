import os
import sys

from shuffle import dinuclShuffle

PATH_TO_GENOME = 'genomes'


def read_genome(path_to_file):
    genome = {}
    with open(os.path.join(f"{path_to_file}"), 'r') as f:
        lines = f.readlines()
        current_id = ""
        for line in lines:
            line = line.strip()
            if line.startswith('>'):
                current_id = line.split(' ')[0].replace('>', '')
                genome[current_id] = ''
                continue
            if line:
                genome[current_id] += line
    return genome


def read_target_genes(path_to_file):
    peak_to_target_gene = {}
    with open(os.path.join(f"{path_to_file}"), 'r') as f:
        for line in f:
            splits = line.split('\t')
            peak_name = splits[3]
            if len(peak_name.split(',')) > 1:
                peak_name = peak_name.split(',')[0][0:-1]
            gene_id = splits[18].split(';')[0].split('-')[1]
            if peak_name not in peak_to_target_gene.keys():
                peak_to_target_gene[peak_name] = [gene_id]
            else:
                peak_to_target_gene[peak_name].append(gene_id)
    return peak_to_target_gene


def run_command(command):
    os.system(command)


def generate_sequences(center_coordinates, genome):
    pos_sequences = []
    for center in center_coordinates:
        # get center before dot
        center = center.split('.')[0]
        center = center.strip()
        center = int(center)
        # get start position
        start = center - 50
        # get end position
        end = center + 50
        # get sequence
        pos_sequences.append(genome[start:end + 1])
    return pos_sequences


def get_paths_to_files(folder_path):
    paths_to_files = {}
    for file in os.listdir(folder_path):
        if file.startswith(".DS_Store"):
            continue
        if file.endswith("_peaks.narrowPeak"):
            paths_to_files["narrow_peaks"] = f"{folder_path}/{file}"
        elif file.endswith("assigned_genes.bed"):
            paths_to_files["assigned_genes"] = f"{folder_path}/{file}"
        else:
            continue
    return paths_to_files


def compose_new_line(line, target_genes):
    line = line.strip()
    peak_name = line.split('\t')[3]
    if peak_name[-1].isalpha():
        peak_name = peak_name[0:-1]
    if peak_name in target_genes.keys():
        line += '\t'
        for gene_id in target_genes[peak_name]:
            line += gene_id + ';'
        line = line[0:-1]
    return line


def main():
    root_path = sys.argv[1]
    genomes_path = sys.argv[2]
    output_dir = sys.argv[3]
    for organism_dir in os.listdir(root_path):
        if organism_dir.startswith('.'):
            continue
        splits = organism_dir.split('-', 2)[2].split('_', 2)
        organism_name = splits[0] + '_' + splits[1]
        genome_id = splits[2]
        genome = read_genome(
            f"{genomes_path}/{organism_name}_{genome_id}.fasta")
        for sample_dir in os.listdir(root_path + '/' + organism_dir):
            if sample_dir.startswith('.'):
                continue
            peak_sequences = []
            peak_info_lines = []
            gene_id = sample_dir.split('_')[0]
            if sample_dir.startswith('Control'):
                continue
            paths_to_files = get_paths_to_files(
                f"{root_path}/{organism_dir}/{sample_dir}")
            path_to_peak_file = paths_to_files["narrow_peaks"]
            target_genes = read_target_genes(paths_to_files["assigned_genes"])
            with open(path_to_peak_file, 'r') as f:
                for line in f:
                    accession_number = line.split('\t')[0]
                    start = line.split('\t')[1]
                    rel_summit_pos = line.split('\t')[9]
                    summit_pos = int(start) + int(rel_summit_pos)
                    sequence = genome[accession_number]
                    pos_sequence = sequence[summit_pos - 50:summit_pos + 50]
                    try:
                        neg_sequence = dinuclShuffle(pos_sequence)
                    except KeyError:
                        print("Cannot shuffle sequence")
                        continue
                    peak_sequences.append(f"{pos_sequence}\t1")
                    peak_sequences.append(f"{neg_sequence}\t0")
                    peak_info_lines.append(compose_new_line(line, target_genes))
            with open(
                    os.path.join(
                        output_dir, f"{organism_name}_{gene_id}_examples.tsv"),
                    'w') as f:
                for seq in peak_sequences:
                    f.write(seq + '\n')
            with open(
                    os.path.join(
                        output_dir, "summary.tsv"),
                    'a') as summary_file:
                num_of_peaks = len(peak_sequences)/2
                summary_file.write(f"{organism_name}\t{gene_id}\t{num_of_peaks}\n")


if __name__ == '__main__':
    main()
