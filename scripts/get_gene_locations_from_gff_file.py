import sys

OUTPUT_FILE_NAME = 'genes_locations'


def main():
    gff_file = sys.argv[1]
    bss_file = sys.argv[2]
    genome_file = sys.argv[3]
    output_dir = sys.argv[4]

    genome = ''
    with open(genome_file, 'r') as f:
        lines = f.readlines()
        for line in lines[1:]:
            if line.startswith('>'):
                break
            genome = genome + line.replace('\n', '')

    genes = {}
    with open(gff_file, 'r') as f:
        lines = f.readlines()
        for line in lines[1:]:
            if line.startswith('NC_003888.3'):
                splits = line.split('\t')
                if splits[8].startswith('ID=gene-SCO'):
                    gene_id = splits[8].split(';')[0].split('-')[1]
                    gene = Gene(gene_id, splits[3], splits[4], splits[6])
                    if gene_id not in genes.keys():
                        genes[gene_id] = gene
                    else:
                        print('Already exists')

    binding_sites = []
    with open(bss_file, 'r') as f:
        lines = f.readlines()
        for line in lines[1:]:
            splits = line.split(',')
            if not splits[1].startswith('SCO'):
                continue
            bs = BindingSite(splits[0], splits[1], splits[3], splits[5],
                             splits[6], splits[7])
            binding_sites.append(bs)

    bss_locations = []
    for bs in binding_sites:
        nearest_gene = genes[bs.gene_id]
        loc_splits = bs.loc.split(':')
        if len(loc_splits) > 1:
            pass
    #         bs_start_location = int(loc_splits[0]) - 1
    #         bs_end_location = int(loc_splits[1])
    #         rev_comp = ''
    #         seq_from_genome = genome[bs_start_location:bs_end_location]
    #         bss_locations.append(
    #             f'{bs.bs_id}\t{bs.gene_id}\t{bs.reg}\t{bs_start_location}\t{bs_end_location}')
    #         if bs.strand == '-':
    #             rev_comp = get_rev_compl(seq_from_genome)
    #         if bs.seq == seq_from_genome:
    #             print('+')
    #         elif bs.seq == rev_comp:
    #             print('+')
    #         else:
    #             print(f'- ({nearest_gene.gene_id})')
    #         print(len(bss_locations))
        else:
            padding = 2*len(bs.seq)
            dist_to_start = abs(int(bs.loc))

            seq_start = int(nearest_gene.start_loc) - dist_to_start - padding
            seq_end = int(nearest_gene.end_loc) + dist_to_start + padding
            seq_from_genome = genome[seq_start:seq_end]

            if bs.seq in seq_from_genome:
                bs_start_location = seq_start + seq_from_genome.find(bs.seq)
                bs_end_location = bs_start_location + len(bs.seq)
                if bs.seq == genome[bs_start_location:bs_end_location]:
                    bss_locations.append(f'{bs.bs_id}\t{bs.gene_id}\t{bs.reg}\t{bs_start_location}\t{bs_end_location}')
            elif bs.get_rev_bs() in seq_from_genome:
                bs_start_location = seq_start + seq_from_genome.find(bs.get_rev_bs())
                bs_end_location = bs_start_location + len(bs.get_rev_bs())
                if bs.get_rev_bs() == genome[bs_start_location:bs_end_location]:
                    bss_locations.append(f'{bs.bs_id}\t{bs.gene_id}\t{bs.reg}\t{bs_start_location}\t{bs_end_location}')
            else:
                print(f'- ({nearest_gene.gene_id})')

    with open(output_dir + '/' + OUTPUT_FILE_NAME + '.tsv', 'w') as f:
        for bs_loc in bss_locations:
            f.write(bs_loc + '\n')


class Gene:
    def __init__(self, gene_id, start_loc, end_loc, strand):
        self.gene_id = gene_id
        self.start_loc = start_loc
        self.end_loc = end_loc
        self.strand = strand

    def __str__(self):
        return f'G: {self.gene_id}, {self.start_loc}, {self.end_loc}, {self.strand}'


class BindingSite:
    def __init__(self, bs_id, gene_id, reg, loc, strand, seq):
        self.bs_id = bs_id
        self.gene_id = gene_id
        self.reg = reg
        self.loc = loc
        self.strand = strand
        self.seq = seq

    def get_rev_bs(self):
        rev_bs = ''
        for bp in self.seq[::-1]:
            if bp == 'A':
                rev_bs += 'T'
            elif bp == 'T':
                rev_bs += 'A'
            elif bp == 'G':
                rev_bs += 'C'
            elif bp == 'C':
                rev_bs += 'G'
        return rev_bs

    def __str__(self):
        return (f'BS: {self.gene_id}, {self.reg}, {self.strand}, {self.loc},'
                f'{self.seq}')


if __name__ == '__main__':
    main()

