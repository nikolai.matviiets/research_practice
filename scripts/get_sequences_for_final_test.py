import sys

SEQ_LENGTH = 101
OUTPUT_FILE_NAME = 'subsequences.tsv'


def main():

    genome_file_name = sys.argv[1]
    file_with_bss_name = sys.argv[2]
    output_folder = sys.argv[3]

    genome = ''
    with open(genome_file_name, 'r') as f_gen:
        lines = f_gen.readlines()
        for line in lines[1:]:
            if line.startswith('>'):
                break
            genome += line.strip()

    bs_locations = {}
    with open(file_with_bss_name, 'r') as f_bs:
        lines = f_bs.readlines()
        for line in lines:
            splits = line.strip().split('\t')
            bs_id = splits[0]
            regulator = splits[2]
            bs_start = int(splits[3])
            bs_end = int(splits[4])
            bs_len = bs_end - bs_start
            summit_pos = bs_start + bs_len // 2
            if regulator not in bs_locations.keys():
                bs_locations[regulator] = [(bs_id, summit_pos)]
            else:
                bs_locations[regulator].append((bs_id, summit_pos))

    for key in bs_locations.keys():
        summits = bs_locations[key]
        with open(output_folder + f'/{key}_' + OUTPUT_FILE_NAME, 'w') as f_out:
            for summit in summits:
                for i in range(-50, 51):
                    centre = int(summit[1]) + i
                    seq = genome[centre-50:centre+51]
                    f_out.write(seq + '\t' + '1' + '\n')


if __name__ == '__main__':
    main()

