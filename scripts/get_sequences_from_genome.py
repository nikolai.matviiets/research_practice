import sys
import random
import collections

MAX_SEQ_LENGTH = 254
OUTPUT_FILE_NAME = 'subsequences.txt'


def main():
    gen_file_name = sys.argv[1]
    data = ''
    with open(gen_file_name, 'r') as f:
        lines = f.readlines()
        for line in lines[1:]:
            data = data + line.replace('\n', '')
    seq_length = len(data)
    sub_sequences = []
    index_of_start = 0
    while index_of_start < seq_length - MAX_SEQ_LENGTH - 1:
        # use max length in 50% of times
        use_max_length = random.randint(0, 1) 
        if use_max_length:
            cur_len = MAX_SEQ_LENGTH
        else:
            cur_len = random.randint(5, MAX_SEQ_LENGTH)
        sub_sequences.append(data[index_of_start:index_of_start+cur_len])
        index_of_start = index_of_start + cur_len
    count_dict = {}
    with open(OUTPUT_FILE_NAME, 'w') as f:
        for seq in sub_sequences:
            f.write(str(seq + '\n'))
            if len(seq) in count_dict.keys():
                count_dict[len(seq)] = count_dict[len(seq)] + 1
            else:
                count_dict[len(seq)] = 1
    od = collections.OrderedDict(sorted(count_dict.items()))
    print(od)


if __name__ == '__main__':
    main()

