import sys
import random

SEQ_LENGTH = 101
OUTPUT_FILE_NAME = 'random_subsequences.txt'


def main():
    gen_file_name = sys.argv[1]
    genome = ''
    with open(gen_file_name, 'r') as f:
        lines = f.readlines()
        for line in lines[1:]:
            genome = genome + line.strip()

    sub_sequences = []
    for i in range(1, 40000):
        start_pos = random.randint(SEQ_LENGTH, len(genome) - SEQ_LENGTH)
        sub_sequences.append(genome[start_pos:start_pos+SEQ_LENGTH])

    with open(OUTPUT_FILE_NAME, 'w') as f:
        for seq in sub_sequences:
            f.write(str(seq + '\t' + '1' + '\n'))


if __name__ == '__main__':
    main()

