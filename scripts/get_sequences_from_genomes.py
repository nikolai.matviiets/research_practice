import sys
import random
import collections
import os

MAX_SEQ_LENGTH = 192
OUTPUT_FILE_NAME = 'subsequences.txt'


def main():
    folder_name = sys.argv[1]
    sequences = []
    for file_name in os.listdir(folder_name):
        if file_name.endswith(".fasta"):
            gen_file_name = os.path.join(folder_name, file_name)
            with open(gen_file_name, 'r') as f:
                lines = f.readlines()
                sequence = ''
                for line_number, line in enumerate(lines[1:], start=1):
                    if line.startswith('>') or line_number == len(lines)-1:
                        if line_number == len(lines)-1:
                            sequence = sequence + line.strip()
                        print(file_name + ", " + str(len(sequence)))
                        sequences.append(sequence)
                        sequence = ''
                    else:
                        sequence = sequence + line.strip()
    for data in sequences:
        seq_length = len(data)
        sub_sequences = []
        index_of_start = 0
        while index_of_start < seq_length - MAX_SEQ_LENGTH - 1:
            # use max length in 50% of times
            use_max_length = random.randint(0, 1)
            if use_max_length:
                cur_len = MAX_SEQ_LENGTH
            else:
                cur_len = random.randint(5, MAX_SEQ_LENGTH)
            sub_sequences.append(data[index_of_start:index_of_start+cur_len])
            index_of_start = index_of_start + cur_len
        count_dict = {}
        with open(folder_name + '/' + OUTPUT_FILE_NAME, 'a') as f:
            for seq in sub_sequences:
                f.write(str(seq + '\n'))
                if len(seq) in count_dict.keys():
                    count_dict[len(seq)] = count_dict[len(seq)] + 1
                else:
                    count_dict[len(seq)] = 1
        od = collections.OrderedDict(sorted(count_dict.items()))
        print(od)


if __name__ == '__main__':
    main()

