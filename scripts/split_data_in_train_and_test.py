import pandas as pd
from sklearn.model_selection import train_test_split

df = pd.read_csv('examples_kmers.tsv', sep='\t', header=None)

training_data, testing_data = train_test_split(df, test_size=0.2,
                                               random_state=25)

# write training data to file
training_data.to_csv('train.tsv', sep='\t', index=False, header=False)

# write testing data to file
testing_data.to_csv('dev.tsv', sep='\t', index=False, header=False)

