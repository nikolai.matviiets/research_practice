def seq2kmer(seq, k):
    """
    Convert original sequence to kmers

    Arguments:
    seq -- str, original sequence.
    k -- int, kmer of length k specified.

    Returns:
    kmers -- str, kmers separated by space

    """
    kmer = [seq[x:x + k] for x in range(len(seq) + 1 - k)]
    kmers = " ".join(kmer)
    return kmers


def main():
    # open file with sequences
    with open('examples.tsv', 'r') as f_input:
        # read line by line
        for line in f_input:
            # get sequence
            seq = line.split()[0]
            label = line.split()[1].strip()
            kmers = seq2kmer(seq, 6)
            # write to file
            with open('examples_kmers.tsv', 'a') as f_ouput:
                f_ouput.write(kmers + '\t' + label + '\n')


if __name__ == '__main__':
    main()